
function PaxSDK() {
  //SDK Initialization
  this.isInitialized;
  this.encryptedData;
  this.paramA;
  this.lat;
  this.long;
  this.operation;
  this.token;
  this.date;
  this.time;
  this.datetime;

  //Initialize transaction
  this.paramB;
  this.paramC;
  this.userName;
  this.deviceSlNo;
  this.amount;

  this.transactionType;

  //Promise Results
  this.txnResult;
  this.deviceData;
  this.pinBlock;
  this.trackData;
  this.cardHolderName;
  this.set8AResult;
  this.set89Result;
  this.set031EResult;
  this.set0308Result
  this.doCashWithdrawalResult;
  this.doBalanceEnquiryResult;


  // let currentdate = new Date();

  // console.log("currentdate", currentdate);

  // let datetime =
  //   currentdate.getUTCFullYear() +
  //   "-" +
  //   ("0" + (currentdate.getUTCMonth() + 1)).slice(-2) +
  //   "-" +
  //   ("0" + currentdate.getUTCDate()).slice(-2) +
  //   " " +
  //   ("0" + currentdate.getUTCHours()).slice(-2) +
  //   ":" +
  //   ("0" + currentdate.getUTCMinutes()).slice(-2) +
  //   ":" +
  //   ("0" + currentdate.getUTCSeconds()).slice(-2);

  // console.log("datetime", datetime);

  // let d = new Date();
  // let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
  // let nd = new Date(utc + (3600000 * +5.5));
  // var ist = nd.toLocaleString();
  // console.log("IST now is : " + ist);


  // let xc = moment().tz("Asia/Kolkata").format();
  // console.log(xc);

  // var currentTime = new Date();

  // var currentOffset = currentTime.getTimezoneOffset();

  // var ISTOffset = 330;   // IST offset UTC +5:30 

  // var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset) * 60000);

  // // ISTTime now represents the time in IST coordinates

  // var hoursIST = ISTTime.getHours()
  // var minutesIST = ISTTime.getMinutes()

  // console.log(hoursIST);
  // console.log(minutesIST);

  // const date = new Date();
  // console.log("current Time", date);
  // var ISToffSet = 330; //IST is 5:30; i.e. 60*5+30 = 330 in minutes 
  // offset = ISToffSet * 60 * 1000;
  // var ISTTime = new Date(date.getTime() + offset);
  // console.log("IST Date", ISTTime);

  // let offset = "+05.53";
  // var b = new Date();
  // var utc = b.getTime() + (b.getTimezoneOffset() * 60000);
  // var nd = new Date(utc + (3600000 * offset));
  // console.log(nd);
  // console.log("the local time", nd.toLocaleString());

  // function getDateAndTime() {
  //   return new Promise((resolve, reject) => {

  //     fetch("http://worldtimeapi.org/api/timezone/Asia/Kolkata")

  //       .then(res => res.json())
  //       .then(ress => {
  //         console.log("datetime", ress);
  //         let nd = ress.datetime;
  //         let arr = nd.split("T");
  //         let arr1 = arr[1].split(".");
  //         let finalArr = arr[0] + " " + arr1[0];
  //         console.log("Gtime", finalArr);
  //         this.datetime = finalArr;
  //         resolve(finalArr);
  //       })
  //       .catch(err => {
  //         console.log(err);
  //         reject(err);
  //       })
  //   })
  // }


  // let x = getDateAndTime().then(ress => x = ress);
  // console.log("done", x);

  //Environment variables
  this.baseUrl = "https://localhost:14000/easyLinkSdkManager";
  this.matmBaseUrl = "https://matm.iserveu.online";
  this.balanceEnquiry = "doBalanceInquiry";
  this.cashWithdrawal = "doCashWithdral";
  this.TPKTDK = "generateTPKandTDK";
  this.dataElements = {
    0: "Message Type Indicator",
    2: "PAN",
    3: "Processing Code",
    4: "Amount",
    7: "Date and Time",
    11: "STAN",
    12: "Local Transaction Time",
    13: "Local Transaction Date",
    22: "POS Entry Mode",
    23: "Card Sequence number",
    24: "Network International Identifier",
    25: "POS Condition code",
    35: "Track 2 Data",
    36: "Transaction Information",
    37: "RRN",
    38: "Authorization Identification Response",
    39: "Response Code",
    41: "Card Acceptor Terminal Information",
    42: "MID",
    43: "Terminal Address",
    48: "Additional Data",
    52: "Pin Block",
    54: "Additional amounts",
    55: "Chip Data",
    61: "POS Data Code",
  };

  const errorCodes = {
    "00": "Approved or completed Successfully",
    "0000": "Approved",
    "03": "Invalid merchant.",
    "04": "Pick-up.",
    "05": "Do not honour. In case CVD, CVD2, iCVD verification fails, Inactive or Dormant account",
    "06": "Error",
    "": "response unknown",
    12: "Invalid transaction or if member is not able to find any appropriate response code",
    13: "Invalid amount.",
    14: "Invalid card number (no such Number).",
    15: "No such issuer.",
    17: "Customer cancellation.",
    20: "Invalid response.",
    21: "No action taken.",
    22: "Suspected malfunction.",
    25: "Unable to locate record",
    27: "File Update field edit error",
    28: "Record already exist in the file",
    29: "File Update not successful",
    30: "Format error.",
    31: "Bank not supported by Switch",
    32: "Partial Reversal",
    33: "Expired card, capture",
    34: "Suspected fraud, capture.",
    36: "Restricted card, capture",
    38: "Allowable PIN tries exceeded, capture.",
    39: "No credit account.",
    40: "Requested function not supported.",
    41: "Lost card, capture.",
    42: "No universal account.",
    43: "Stolen card, capture.",
    51: "Not sufficient funds.",
    52: "No checking account",
    53: "No savings account.",
    54: "Expired card, decline",
    55: "Incorrect personal identification number.",
    56: "No card record.",
    57: "Transaction not permitted to Cardholder",
    58: "Transaction not permitted to terminal.",
    59: "Suspected fraud, decline / Transactions declined based on Risk Score",
    60: "Card acceptor contact acquirer, decline.",
    61: "Exceeds withdrawal amount limit.",
    62: "Restricted card, decline.",
    63: "Security violation.",
    65: "Exceeds withdrawal frequency limit.",
    66: "Card acceptor calls acquirer’s.",
    67: "Hard capture (requires that card be picked up at ATM)",
    68: "Acquirer time-out",
    69: "Mobile number record not found/ mis-match",
    71: "Deemed Acceptance",
    74: "Transactions declined by Issuer based on Risk Score",
    75: "Allowable number of PIN tries exceeded, decline",
    81: "Cryptographic Error",
    90: "Cut-off is in process.",
    91: "Issuer or switch is inoperative",
    92: "No routing available",
    93: "Transaction cannot be completed. Compliance violation.",
    94: "Duplicate transmission.",
    95: "Reconcile error",
    96: "System malfunction",
    E3: "ARQC validation failed by Issuer",
    E4: "TVR validation failed by Issuer",
    E5: "CVR validation failed by Issuer",
    MU: "No Aadhar linked to Card",
    UG: "INVALID BIOMETRIC DATA",
    U3: "BIOMETRIC DATA DID NOT MATCH",
    WZ: "Technical Decline UIDAI",
    CA: "Compliance error code for acquirer",
    CI: "Compliance error code for issuer",
  };
  // const responseCodes = {
  //   "00": "APPROVED",
  //   "03": "Invalid merchant",
  //   "04": "Pick-up",
  //   "05": "Inactive or Dormant account",
  //   "06": "Error",
  // };

  // this.setParamBC = (packet) => {
  //   const { paramB, paramC } = packet;
  //   this.paramB = paramB;
  //   this.paramC = paramC;
  // };

  this.authenticateData = () => {
    return new Promise((resolve, reject) => {
      fetch(`https://mobile.9fin.co.in/api/getAuthenticateData`, {
        method: "post",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({
          encryptedData: this.encryptedData,
          retailerUserName: this.paramA,
        }),
      })
        .then((result) => {
          result.json().then((data) => {
            if (data.status == "success") {
              console.log(data.usertoken);
              this.token = data.usertoken;
              resolve(data);
            } else {
              reject(data);
            }
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  this.getCurrentLocation = () => {
    // return new Promise((resolve, reject) => {
    if (navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude || "";
        this.long = position.coords.longitude || "";
      });
    }
  };

  this.hex_to_ascii = (str1) => {
    var hex = str1.toString();
    var str = "";
    for (var n = 0; n < hex.length; n += 2) {
      str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
  };

  this.fetchTxnDate = () => {
    const d = new Date(Date.now());
    const yy = `${d.getFullYear()}`.substr(2);
    const mm = d.getMonth();
    const dd = d.getDate();
    const date = `${yy}${mm + 1 >= 10 ? mm + 1 : "0" + (mm + 1)}${dd >= 10 ? dd : "0" + dd
      }`;
    return date;
  };

  this.fetchTxnTime = () => {
    const d = new Date(Date.now());
    const hh = d.getHours();
    const mm = d.getMinutes();
    const ss = d.getSeconds();
    const time = `${hh >= 10 ? hh : "0" + hh}${mm >= 10 ? mm : "0" + mm}${ss >= 10 ? ss : "0" + ss
      }`;
    return time;
  };
  this.generateTxnAmount = (amount) => {
    const finalAmount = amount * 100;
    const trailingZeroes = 12 - `${finalAmount}`.length;
    const txnAmount = `${"0".repeat(trailingZeroes)}${finalAmount}`;
    return txnAmount;
  };

  this.getDateAndTime = () => {
    return new Promise((resolve, reject) => {

      fetch("http://worldtimeapi.org/api/timezone/Asia/Kolkata")

        .then(res => res.json())
        .then(ress => {
          console.log("datetime", ress);
          let nd = ress.datetime;
          let arr = nd.split("T");
          let arr1 = arr[1].split(".");
          let finalArr = arr[0] + " " + arr1[0];
          console.log("Gtime", finalArr);
          this.datetime = finalArr;
          resolve();
        })
        .catch(err => {
          console.log(err);
          reject(err);
        })

    })

  }


  this.fetchDate = () => {
    return new Promise((resolve, reject) => {

      fetch("http://worldtimeapi.org/api/timezone/Asia/Kolkata")

        .then(res => res.json())
        .then(ress => {
          console.log("datetime", ress);
          let nd = ress.datetime;
          let arr = nd.split("T");
          console.log("Global Date:", arr[0]);
          this.date = arr[0];
          resolve();
        })
        .catch(err => {
          console.log(err);
          reject(err);
        })

    })
  }

  this.fetchTime = () => {
    return new Promise((resolve, reject) => {

      fetch("http://worldtimeapi.org/api/timezone/Asia/Kolkata")

        .then(res => res.json())
        .then(ress => {
          console.log("datetime", ress);
          let nd = ress.datetime;
          let arr = nd.split("T");
          let arr1 = arr[1].split(".");
          console.log("Gtime", arr1[0]);
          this.time = arr1[0];
          resolve();
        })
        .catch(err => {
          console.log(err);
          reject(err);
        })

    })
  }
  // this.getDateAndTime();

  this.getDeviceSL = () => {
    return new Promise((resolve, reject) => {
      console.log("Getting device serial no...");
      const tag = `0101`;
      fetch(
        `${this.baseUrl}/getData/1?datatype=CONFIGURATION_DATA&tag=${tag}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.deviceSlNo = this.hex_to_ascii(data.data.substr(10));
            console.log(`Device sl no. ${this.deviceSlNo}`);
            resolve();
          } else {
            reject(data);
            //Conditional success
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.fetchKeys = (deviceSlNo) => {
    return new Promise((resolve, reject) => {
      fetch(
        // `${this.matmBaseUrl}/generateTPKandTDK/${deviceSlNo}/${"clientUsername"}`,
        `${this.matmBaseUrl}/generateTPKandTDK/${deviceSlNo}/${this.userName}`,
        {
          method: "post",
          headers: {
            "Content-Type": "application/json",
            authorization: this.token,
            "Access-Control-Allow-Origin": "*",
          },
        }
      )
        .then((result) => result.json())
        .then((data) => {
          // console.log(data);
          if (data.status == 0) {
            resolve(data);
          } else {
            reject(data);
          }
        })
        .catch((err) => {
          reject(err);
        });

      //  resolve({
      //   tpk: "0C3A7DD4DEEF8720829FA1A8B04A9D39",
      //   tdk: "440301643582A2339A2DD702C80AF5C8",
      //  });
    });
  };
  this.setTPK = (key) => {
    return new Promise((resolve, reject) => {
      // const tag = `57`;
      fetch(
        `${this.baseUrl}/WriteTWK?srcKeyType=PED_TMK&srcKeyIndex=30&dstKeyType=PED_TPK&dstKeyIndex=32&dstKeyValue=${key}&checkMode=0&timeoutSeconds=20`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            //Success
            resolve(data);
          } else {
            reject(data);
            //Conditional success
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.setTDK = (key) => {
    return new Promise((resolve, reject) => {
      // const tag = `57`;
      fetch(
        `${this.baseUrl}/WriteTWK?srcKeyType=PED_TMK&srcKeyIndex=30&dstKeyType=PED_TDK&dstKeyIndex=37&dstKeyValue=${key}&checkMode=0&timeoutSeconds=20`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            //Success
            resolve(data);
          } else {
            resolve(data);
            //Conditional success
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.checkDeviceMapping = () => {
    console.log("Check Device API called");
    return new Promise((resolve, reject) => {
      console.log("Checking device mapping...");
      console.log(this.userName);
      console.log(this.paramA);
      fetch(
        `https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/matmmapping/mapwithusername`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            userName: this.userName,
            deviceSlNo: this.deviceSlNo,
          }),
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.status == 0) {
            console.log(`Device mapping success`);
            resolve();
          } else if (data.status == 1) {
            console.log(`Device mapping error`);
            reject(data.desc);
          }
        })
        .catch((err) => {
          console.log(`Device mapping error`, err);
          reject(`Device mapping error`);
        });
    });
  };
  this.injectKeys = () => {
    return new Promise((resolve, reject) => {
      console.log(`Injecting TPK and TDK`);
      this.fetchKeys(this.deviceSlNo).then((keyResult) => {
        // console.log(keyResult);
        this.setTPK(keyResult.tpk)
          .then((tpkResult) => {
            console.log("TPK Injected");
            this.setTDK(keyResult.tdk)
              .then((tdkResult) => {
                console.log("TDK Injected");
                resolve();
              })
              .catch((err) => {
                reject(`Key Injection error!`);
              });
          })
          .catch((err) => {
            reject(`Key Injection error!`);
          });
      });
    });
  };
  this.getTrackData = () => {
    return new Promise((resolve, reject) => {
      console.log("Getting track data ...");
      const tag = `57`;
      fetch(`${this.baseUrl}/getData/1?datatype=TRANSACTION_DATA&tag=${tag}`, {
        method: "post",
      })
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            //Success
            this.trackData = data.data.substr(8);
            // console.log(data);
            resolve(data);
          } else {
            resolve(data);
            //Conditional success
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  this.get5F20Data = () => {
    return new Promise((resolve, reject) => {
      console.log("getting 5f20 data...");
      const tag = "5f20";
      fetch(`${this.baseUrl}/getData/1?datatype=TRANSACTION_DATA&tag=${tag}`, {
        method: "post",
      })

        .then((result) => result.json())
        .then((data) => {
          console.log(data);
          if (data.code == 0) {
            this.cardHolderName = this.hex_to_ascii(data.data.substr(10))
            console.log(this.cardHolderName);
            resolve();
          } else {
            console.log("else block ");
            console.log(data);
            reject()
          }
        })
        .catch(err => {
          reject(err);
        })
    })
  }
  // this.getPAN = () => {
  //   return new Promise((resolve, reject) => {
  //     const tag = `5A`;
  //     fetch(`${this.baseUrl}/getData/1?datatype=TRANSACTION_DATA&tag=${tag}`, {
  //       method: "post",
  //     })
  //       .then((result) => result.json())
  //       .then((data) => {
  //         if (data.code == 0) {
  //           //Success
  //           resolve(data);
  //         } else {
  //           resolve(data);
  //           //Conditional success
  //         }
  //       })
  //       .catch((err) => {
  //         reject(err);
  //       });
  //   });
  // };

  this.parseResponse = (statusDesc, txanId, txn_card_type, cardHolderName) => {
    // const data = JSON.parse(pData);
    const packet = statusDesc;
    const txnId = txanId;
    let response = {};
    if (packet[54]) {
      let cryptedAmount = packet[54];
      //  let leftAmount = cryptedAmount.substr(0, 20);
      let rightAmount = cryptedAmount.substr(20, cryptedAmount.length);
      let rightBalanceAmount = rightAmount.substr(7);
      if (rightBalanceAmount.startsWith("C")) {
        response.BALANCE = parseInt(rightBalanceAmount.substr(1)) / 100;
      } else {
        response.BALANCE = -parseInt(rightBalanceAmount.substr(1)) / 100;
      }
    } else {
      response.BALANCE = "N/A";
    }

    if (packet[39]) {
      response.RESPONSE_CODE = packet[39];
      const resCode = packet[39].substr(2);
      if (resCode == "00") {
        response.STATUS = "SUCCESS";
        response.RESPONSE_CODE = resCode || "N/A";
        response.REMARKS = "Transaction Successful";
        response.MOBILE_NO = this.paramC;
      } else {
        response.STATUS = "FAILED";
        response.RESPONSE_CODE = resCode || "N/A";
        response.REMARKS = errorCodes[resCode] || "N/A";
        response.MOBILE_NO = this.paramC;
      }
    }

    if (packet[37]) {
      response.RRN = packet[37] || "N/A";
    }

    if (packet[41]) {
      response.MID = packet[41] || "N/A";
    }

    if (packet[42]) {
      response.TID = packet[42] || "N/A";
    }

    if (packet[35]) {
      response.CARD_NUMBER = packet[35] || "N/A";
    }

    if (txnId) {
      response.TXN_ID = txnId || "N/A";
    }
    response.TRANSACTION_TYPE = this.transactionType || "N/A";
    response.TRANSACTION_AMOUNT = this.amount || 0;

    // const d = new Date(Date.now());
    // const time = d.toLocaleString();

    response.TRANSATION_TIME = this.datetime;
    // response.CARD_HOLDER_NAME = (this.hex_to_ascii(this.get5F20Data())) ? this.hex_to_ascii(this.get5F20Data()) : "N/A";
    response.CARD_TYPE = txn_card_type;
    response.CARD_HOLDER_NAME = cardHolderName;
    return response;
  };


  this.doBalanceEnquiry = () => {
    console.log("Balance enquiry API called");
    this.transactionType = "BALANCE ENQUIRY";
    return new Promise((resolve, reject) => {

      // const txnDate = this.fetchTxnDate();
      const obj = {
        pansn: "1",
        amount: this.generateTxnAmount(0),
        trackData: this.trackData,
        deviceSerial: this.deviceSlNo,
        pinblock: this.pinBlock,
        deviceData: this.deviceData + "9F0306" + this.generateTxnAmount(0),
        transactionType: "31",
        isFallback: "no",
        txn_card_type: "",
        txnLatitude: this.lat,
        txnLongitude: this.long,
        paramA: this.paramA || "",
        paramB: this.paramB || "",
        paramC: this.paramC || "",
        // retailer: clientUsername,
        retailer: this.userName,
        isSL: true,
        timestamp: this.datetime,
        cardHolderName: this.cardHolderName,
        clientInfo: "{\"deviceType\":\"PAX D180\",\"platformType\":\"web.sdk\",\"deviceSerialNo\":" + this.deviceSlNo + "\"AppVersion\":\"1.0.0.4\"}",
      };

      console.log("req of dobalance enquiry ", obj);

      axios
        .post(`${this.matmBaseUrl}/doBalanceInquiry`, obj, {
          headers: {
            "Content-Type": "application/json",
            authorization: this.token,
            "Access-Control-Allow-Origin": "*",
          },
          transformResponse: (data) => JsonBigint.parse(data),
        })
        .then((result) => {
          if (result.data.status == 0) {
            this.doBalanceEnquiryResult = result.data;
            resolve();
          } else {
            reject(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
          reject();
        });

      // fetch(`${this.matmBaseUrl}/doBalanceInquiry`, {
      //   method: "post",
      //   headers: {
      //     "Content-Type": "application/json",
      //     authorization: this.token,
      //     "Access-Control-Allow-Origin": "*",
      //   },
      //   body: JSON.stringify(obj),
      // })
      //   .then((result) => result.json())
      //   .then((data) => {
      //     if (data.status == 0) {
      //       console.log(data);
      //       this.doBalanceEnquiryResult = data;

      //       resolve(data);
      //     } else {
      //       reject();
      //     }
      //   })
      //   .catch((err) => {
      //     console.log(err);
      //     reject(err);
      //   });
    });
  };

  this.doCashWithdrawal = (packet) => {
    console.log("Balance withdrawal API called");
    this.transactionType = "CASH WITHDRAWAL";
    return new Promise((resolve, reject) => {
      const { amount } = packet;
      // const txnDate = this.fetchTxnDate();

      const obj = {
        pansn: "1",
        amount: this.generateTxnAmount(amount),
        trackData: this.trackData,
        deviceSerial: this.deviceSlNo,
        pinblock: this.pinBlock,
        deviceData: this.deviceData + "9F0306" + this.generateTxnAmount(0),
        transactionType: "01",
        isFallback: "no",
        txn_card_type: "",
        txnLatitude: this.lat | "",
        txnLongitude: this.long | "",
        paramA: this.paramA || "",
        paramB: this.paramB || "",
        paramC: this.paramC || "",
        // retailer: clientUsername,
        retailer: this.userName,
        isSL: true,
        timestamp: this.datetime,
        cardHolderName: this.hex_to_ascii(this.get5F20Data()),
        clientInfo: "{\"deviceType\":\"PAX D180\",\"platformType\":\"web.sdk\",\"deviceSerialNo\":" + this.deviceSlNo + "\"AppVersion\":\"1.0.0.4\"}",
      };

      axios
        .post(`${this.matmBaseUrl}/doCashWithdral`, obj, {
          headers: {
            "Content-Type": "application/json",
            authorization: this.token,
            "Access-Control-Allow-Origin": "*",
          },
          transformResponse: (data) => JsonBigint.parse(data),
        })
        .then((result) => {
          if (result.data.status == 0) {
            this.doCashWithdrawalResult = result.data;
            resolve();
          } else {
            reject(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
          showAlert(
            "Error",
            err.statusDes ? err.statusDes : "Some Problem Occoured",
            tempModalElem.options.alert
          );
          hideLoader();
          reject(err);
        });

      // fetch(`${this.matmBaseUrl}/doCashWithdral`, {
      //   method: "post",
      //   headers: {
      //     "Content-Type": "application/json",
      //     authorization: this.token,
      //     "Access-Control-Allow-Origin": "*",
      //   },
      //   body: JSON.stringify(obj),
      // })
      //   .then((result) => result.json())
      //   .then((data) => {
      //     if (data.status == 0) {
      //       this.doCashWithdrawalResult = data;
      //       // console.log(data);
      //       resolve(data);
      //     } else {
      //       reject();
      //     }
      //   })
      //   .catch((err) => {
      //     console.log("err", err);
      //     reject(err);
      //   });
    });
  };

  this.connectDevice = () => {
    return new Promise((resolve, reject) => {
      console.log("Connecting device...");
      fetch(`${this.baseUrl}/connectDevice/1?commtype=USB&connectTimeout=20`)
        .then((result) => result.json())
        .then((data) => {
          if (
            data.code == 0 ||
            data.code == 1001 ||
            data.msg === "success" ||
            data.msg === "Already connected"
          ) {
            resolve();
          } else {
            if (data.msg === "Comm connect err") {
              reject("Comm connect err");
            }
          }
        })
        .catch((err) => {
          reject(`Device connection error!`);
        });
    });
  };
  this.isConnected = () => {
    return new Promise((resolve, reject) => {
      console.log("Checking device connection...");
      fetch(`${this.baseUrl}/isConnected`)
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            if (data.data === "not connected" || data.data === "connected") {
              resolve();
            }
          } else {
            reject(data.msg || "Device connection error");
          }
        })
        .catch((err) => {
          reject(
            "Please check if ElWeb Service is running and device screen is on"
          );
        });
    });
  };
  this.disconnect = () => {
    return new Promise((resolve, reject) => {
      fetch(`${this.baseUrl}/disconnect`)
        .then((result) => result.json())
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.setTxnData = () => {
    return new Promise((resolve, reject) => {
      console.log("Setting transaction data...");
      let type, amount;
      if (this.operation === "CW") {
        type = "01";
        amount = this.amount;
      }
      if (this.operation === "BE") {
        type = "31";
        amount = 0;
      }
      const txnAmountTLV = "9F0206" + this.generateTxnAmount(amount);

      const txnTypeTLV = `9C01${type}`;
      const txnCurrCodeTLV = `5F2A020356`;
      const txnCurrExpTLV = `5F360102`;
      // const txnDate = this.fetchTxnDate();
      // const txnTime = this.date;
      // const datetime1 = this.time;

      //const concatTLV = `${txnAmountTLV}${txnTypeTLV}${txnCurrCodeTLV}${txnCurrExpTLV}9A03${txnDate}9F2103${txnTime}`;
      const concatTLV = `${txnAmountTLV}${txnTypeTLV}${txnCurrCodeTLV}${txnCurrExpTLV}`;
      console.log(concatTLV);
      fetch(
        `${this.baseUrl}/setData/1?datatype=TRANSACTION_DATA&tlvData=${concatTLV}`,
        { method: "post" }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            resolve(data);
          } else {
            resolve(data);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.setConfigData = () => {
    return new Promise((resolve, reject) => {
      console.log(`Setting config data...`);
      const concatTLV = `0202010102050101020301200206012502040100020901010214010702070101`;
      fetch(
        `${this.baseUrl}/setData/1?datatype=CONFIGURATION_DATA&tlvData=${concatTLV}`,
        { method: "post" }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            resolve(data);
            //Success
          } else {
            resolve(data);
            //Conditional success
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  this.startTransaction = () => {
    return new Promise((resolve, reject) => {
      console.log("Starting transaction...");
      // showAlert2(
      //   "Insert card",
      //   "Please insert a card and enter pin to proceed and don't refresh the page ",
      //   tempModalElem.options.alert
      // );
      fetch(`${this.baseUrl}/startTransaction`)
        .then((result) => result.json())
        .then((data) => {
          console.log(data);
          if (data.code == 0) {
            // hideAlert();
            resolve(data);
            //Success
          } else {
            reject(data);
            hideAlert();
            //Conditional success
          }
        })
        .catch((err) => {
          reject(`Start transaction error!`);
        });
    });
  };
  this.getCardType = () => {
    return new Promise((resolve, reject) => {
      console.log(`Getting card type...`);
      const tag = `0301`;
      fetch(
        `${this.baseUrl}/getData/1?datatype=CONFIGURATION_DATA&tag=${tag}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            resolve(data);
          } else {
            console.log((`Card type error!`, data));
            reject();
          }
        })
        .catch((err) => {
          console.log((`Card type error!`, err));
          reject();
        });
    });
  };
  this.getTxnResult = () => {
    return new Promise((resolve, reject) => {
      console.log(`Getting transaction result...`);
      const tag = `031B`;
      fetch(
        `${this.baseUrl}/getData/1?datatype=CONFIGURATION_DATA&tag=${tag}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            if (data.data.substr(10) === "02") {
              this.txnResult = true;
              resolve();
            } else {
              console.log("Transaction result failed!");
              reject();
            }
            //Success
          } else {
            console.log(`Get Txn result error!`, data);
            reject();
            //Conditional success
          }
        })
        .catch((err) => {
          console.log(`Get Txn result error!`, err);
          reject();
        });
    });
  };

  this.getDeviceData = () => {
    return new Promise((resolve, reject) => {
      console.log(`Getting device data...`);
      // const tag = `8284959F029B9F269A9C5F2A9F339F1A9F1E9F279F369F379F349F10`;
      const tag = `8284959F029B9F269A9C5F2A9F339F1A9F1E9F279F369F379F349F10`;
      fetch(`${this.baseUrl}/getData/1?datatype=TRANSACTION_DATA&tag=${tag}`, {
        method: "post",
      })
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.deviceData = data.data.substr(4);
            resolve();
          } else {
            console.log(`Get device data error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Get device data error!`, err);
          reject();
        });
    });
  };

  this.getPinBlock = () => {
    return new Promise((resolve, reject) => {
      console.log(`Getting pin block...`);
      const tag = `0316`;
      fetch(
        `${this.baseUrl}/getData/1?datatype=CONFIGURATION_DATA&tag=${tag}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.pinBlock = data.data.substr(10);
            resolve();
          } else {
            console.log(`Get device data error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Get device data error!`, err);
          reject();
        });
    });
  };

  this.completeTransactionISU = (msg, txnId) => {
    return new Promise((resolve, reject) => {
      console.log("completed the transaction process.");
      let statusId;
      if (this.operation == "BE") {
        let parsed = JSON.parse(this.doBalanceEnquiryResult.statusDes);
        if (parsed["39"] == "0000") {
          statusId = "APPROVED";
        } else {
          statusId = "DECLINE";
        }
      }
      if (this.operation == "CW") {
        let parsed = JSON.parse(this.doCashWithdrawalResult.statusDes);
        if (parsed["39"] == "0000") {
          statusId = "APPROVED";
        } else {
          statusId = "DECLINE";
        }
      }

      let obj = { id: txnId, status: statusId };
      fetch(`${this.matmBaseUrl}/completetransaction`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          authorization: this.token,
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify(obj),
      })
        .then((result) => result.json())
        .then((data) => {
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  };

  this.completeTransaction = () => {
    return new Promise((resolve, reject) => {
      console.log("Completing transaction...");
      fetch(`${this.baseUrl}/completeTransaction`)
        .then((result) => result.json())
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  this.set8A = (packet) => {
    return new Promise((resolve, reject) => {
      console.log(`Setting 8A...`);
      fetch(
        `${this.baseUrl}/setData/1?datatype=TRANSACTION_DATA&tlvData=8A02${packet}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.set8AResult = data;
            resolve();
          } else {
            console.log(`Set 8A error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Set 8A error!`, err);
          reject();
        });
    });
  };

  this.set89 = (packet) => {
    return new Promise((resolve, reject) => {
      console.log(`Setting 89...`);
      fetch(
        `${this.baseUrl}/setData/1?datatype=TRANSACTION_DATA&tlvData=8903${packet}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.set89Result = data;
            resolve();
          } else {
            console.log(`Set 89 error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Set 89 error!`, err);
          reject();
        });
    });
  };
  this.set031E = (packet) => {
    return new Promise((resolve, reject) => {
      console.log(`Setting 031E...`);
      fetch(
        `${this.baseUrl}/setData/1?datatype=TRANSACTION_DATA&tlvData=${packet}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.set031EResult = data;
            resolve();
          } else {
            console.log(`Set 031E error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Set 031E error!`, err);
          reject();
        });
    });
  };

  this.set0308 = (packet) => {
    return new Promise((resolve, reject) => {
      console.log(`Setting 0308...`);
      fetch(
        `${this.baseUrl}/setData/2?datatype=CONFIGURATION_DATA&tags=0308&values=${packet}`,
        {
          method: "post",
        }
      )
        .then((result) => result.json())
        .then((data) => {
          if (data.code == 0) {
            this.set0308Result = data;
            console.log(data);
            resolve();
          } else {
            console.log(`Set 0308 error!`, data);
            reject();
          }
        })
        .catch((err) => {
          console.log(`Set 0308 error!`, err);
          reject();
        });
    });
  }

  //Not used currently
  // this.getConnectedDevice = () => {
  //   return new Promise((resolve, reject) => {
  //     fetch(`${this.baseUrl}/getConnectedDevice`)
  //       .then((result) => result.json())
  //       .then((data) => {
  //         resolve(data);
  //       })
  //       .catch((err) => {
  //         reject(err);
  //       });
  //   });
  // };

  this.cancelTxn = () => {
    return new Promise((resolve, reject) => {
      fetch(`${this.baseUrl}/cancelTrans`)
        .then((result) => result.json())
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  this.initiateProcess = (packet) => {
    const { amount, type } = packet;
    return new Promise((resolve, reject) => {
      this.isConnected()
        .then(this.connectDevice)
        .then(this.getDeviceSL)
        .then(this.checkDeviceMapping)
        .then(this.injectKeys)
        .then(this.setConfigData)
        .then(this.setTxnData)
        .then(this.startTransaction)
        .then(this.getCardType)
        .then(this.get5F20Data)
        .then((cardType) => {
          // console.log(cardType);
          let card = `${cardType}`.substr(10);
          if (card === "01") {
            console.log(`Card type 01`);
            this.getTxnResult()
              .then(this.getDeviceData)
              .then(this.getTrackData)
              .then(this.getPinBlock)
              .then(this.getDateAndTime)
              .then(() => {
                if (type == 0) {
                  this.doCashWithdrawal({
                    amount: this.amount,
                  })
                    .then(this.completeTransaction)
                    //new added
                    .then((data) => {
                      this.completeTransactionISU(
                        data.msg,
                        this.doCashWithdrawalResult.txnId
                      )
                        //till this new
                        .then(() => {
                          const response = this.parseResponse(
                            this.doCashWithdrawalResult
                          );
                          resolve(response);
                        })
                        .catch((err) => {
                          showAlert(
                            "Error",
                            err.statusDes
                              ? err.statusDes
                              : "Some Problem Occoured",
                            tempModalElem.options.alert
                          );
                          hideLoader();
                        });
                    });
                } else if (type == 1) {
                  this.doBalanceEnquiry()
                    // this.doBalanceEnquiry()
                    .then(this.completeTransaction)
                    .then((data) => {
                      // console.log(data);
                      this.completeTransactionISU(
                        data.msg,
                        this.doBalanceEnquiryResult.txnId
                      ).then(() => {
                        const response = this.parseResponse(
                          this.doBalanceEnquiryResult
                        );
                        resolve(response);
                      });
                    });
                }
              });
          } else {
            this.getTxnResult().then(() => {
              if (this.txnResult) {
                this.getDeviceData()
                  .then(this.getTrackData)
                  .then(this.getPinBlock)
                  .then(this.getDateAndTime)
                  .then(() => {
                    if (type == 0) {
                      this.doCashWithdrawal({
                        amount: this.amount,
                      })
                        .then(() => {
                          console.log(this.doCashWithdrawalResult);
                          if (this.doCashWithdrawalResult.status == -1) {
                            reject("Error while processing withdraw request");
                          } else if (this.doCashWithdrawalResult.status == 0) {
                            let parsed = JSON.parse(
                              this.doCashWithdrawalResult.statusDes
                            );
                            if (parsed["39"].substr(2) == "00") {
                              this.set8A(parsed["39"])
                                .then(this.set89(parsed["38"]))
                                .then(this.set031E(parsed["55"]))
                                .then(this.set0308("00"))
                                .then(this.completeTransaction)
                                .then((data) => {
                                  // console.log(data);
                                  this.completeTransactionISU(
                                    data.msg,
                                    this.doCashWithdrawalResult.txnId
                                  )
                                    .then((sucResult) => {
                                      let parsedDesc = JSON.parse(
                                        this.doCashWithdrawalResult.statusDes
                                      );
                                      const response = this.parseResponse(
                                        parsedDesc,
                                        this.doCashWithdrawalResult.txnId,
                                        this.doCashWithdrawalResult.txn_card_type,
                                        this.doCashWithdrawalResult.cardHolderName
                                      );
                                      resolve(response);
                                    })
                                    .catch((sucErr) => {
                                      throw new Error(sucErr);
                                    });
                                });
                            } else {
                              this.set8A(parsed["39"])
                                .then(this.set89(parsed["38"]))
                                .then(this.set031E(this.deviceData))
                                .then(this.set0308("00"))
                                .then(this.completeTransaction)
                                .then((data) => {
                                  // console.log(data);
                                  this.completeTransactionISU(
                                    data.msg,
                                    this.doCashWithdrawalResult.txnId
                                  )
                                    .then((sucResult) => {
                                      let parsedDesc = JSON.parse(
                                        this.doCashWithdrawalResult.statusDes
                                      );
                                      const response = this.parseResponse(
                                        parsedDesc,
                                        this.doCashWithdrawalResult.txnId,
                                        this.doCashWithdrawalResult.txn_card_type,
                                        this.doCashWithdrawalResult.cardHolderName
                                      );
                                      resolve(response);
                                    })
                                    .catch((sucErr) => {
                                      throw new Error(sucErr);
                                    });
                                });
                            }
                          }
                        })
                        .catch((err) => {
                          // showAlert("Error", err.statusDes?err.statusDes:"Some Problem Occoured", tempModalElem.options.alert);
                          // hideLoader()
                          // console.log("execute");
                        });
                    } else if (type == 1) {
                      this.doBalanceEnquiry()
                        .then(() => {
                          console.log(this.doBalanceEnquiryResult);
                          if (this.doBalanceEnquiryResult.status == -1) {
                            reject("Error while processing request");
                          } else if (this.doBalanceEnquiryResult.status == 0) {
                            let parsed = JSON.parse(
                              this.doBalanceEnquiryResult.statusDes
                            );
                            if (parsed["39"].substr(2) == "00") {
                              this.set8A(parsed["39"])
                                .then(this.set89(parsed["38"]))
                                .then(this.set031E(parsed["55"]))
                                .then(this.set0308("00"))
                                .then(this.completeTransaction)
                                .then((data) => {
                                  // console.log(data);
                                  this.completeTransactionISU(
                                    data.msg,
                                    this.doBalanceEnquiryResult.txnId
                                  )
                                    .then((sucResult) => {
                                      let parsedDesc = JSON.parse(
                                        this.doBalanceEnquiryResult.statusDes
                                      );
                                      const response = this.parseResponse(
                                        parsedDesc,
                                        this.doBalanceEnquiryResult.txnId,
                                        this.doBalanceEnquiryResult.txn_card_type,
                                        this.doBalanceEnquiryResult.cardHolderName
                                      );
                                      resolve(response);
                                    })
                                    .catch((sucErr) => {
                                      throw new Error(sucErr);
                                    });
                                });
                            } else {
                              this.set8A(parsed["39"])
                                .then(this.set89(parsed["38"]))
                                .then(this.set031E(this.deviceData))
                                .then(this.set0308("00"))
                                .then(this.completeTransaction)
                                .then((data) => {
                                  this.completeTransactionISU(
                                    data.msg,
                                    this.doBalanceEnquiryResult.txnId
                                  )
                                    .then((sucResult) => {
                                      let parsedDesc = JSON.parse(
                                        this.doBalanceEnquiryResult.statusDes
                                      );
                                      const response = this.parseResponse(
                                        parsedDesc,
                                        this.doBalanceEnquiryResult.txnId,
                                        this.doBalanceEnquiryResult.txn_card_type,
                                        this.doBalanceEnquiryResult.cardHolderName
                                      );
                                      resolve(response);
                                    })
                                    .catch((sucErr) => {
                                      throw new Error(sucErr);
                                    });
                                });
                            }
                          }
                        })
                        .catch((err) => {
                          console.log(err);
                          // showAlert(
                          //   "Error",
                          //   err.statusDes,
                          //   tempModalElem.options.alert
                          // );
                          // hideLoader();
                        });
                    }
                  });
              }
            });
          }
        })
        .catch((err) => {
          console.log(err);
          // showAlert(
          //   "Error",
          //   err.msg ? err.msg : err,
          //   tempModalElem.options.alert
          // );
          // hideLoader();
          // throw new Error(err);
        });
    });
  };

  this.intiateCashWithdrawal = (amount, mobNo) => {
    return new Promise((resolve, reject) => {
      if (this.isInitialized) {
        this.operation = "CW";
        this.paramC = mobNo;
        //   if (!Number.isInteger(amount)) {
        // 	reject("Amount must be a number");
        //   }
        //   if (!(amount <= 10000 && amount >= 100)) {
        // 	reject("Amount must be between 1 and 10000");
        //   }
        this.amount = amount;
        this.userName = this.paramA;
        this.initiateProcess({
          type: 0,
          userName: this.paramA,
          amount: this.amount,
        })
          .then((result) => {
            console.log(result);
            resolve(result);
          })
          .catch((err) => {
            showAlert("Error", err, tempModalElem.options.alert);
            hideLoader();
            throw new Error(err);
          });
      } else {
        console.log("SDK initializing failed!");
      }
    });
  };

  this.initiateBalanceEnquiry = (mobNo) => {
    return new Promise((resolve, reject) => {
      if (this.isInitialized) {
        this.operation = "BE";
        this.userName = this.paramA;
        this.paramC = mobNo;
        this.initiateProcess({ type: 1, userName: this.paramA, amount: 0 })
          .then((result) => {
            console.log(result);
            resolve(result);
          })
          .catch((err) => {
            throw new Error(err);
          });
      } else {
        console.log("SDK initializing failed!");
      }
    });
  };

  // this.initiateMiniStatement = (packet) => {
  //   if (this.isInitialized) {
  //     this.initiateProcess(packet)
  //       .then((result) => {})
  //       .catch((err) => {});
  //   } else {
  //     console.log("SDK initializing failed!");
  //   }
  // };

  this.init = (packet) => {
    console.log("Init called");
    const { encryptedData, paramA, paramB, paramC } = packet;
    // showLoader();
    this.encryptedData = encryptedData;
    this.paramA = paramA || ""; //userName
    this.paramB = paramB || ""; //txn_id
    this.paramC = paramC ? paramC : "";
    this.getCurrentLocation();
    this.authenticateData()
      .then((result) => {
        if (result.status === "success") {
          this.isInitialized = true;
          // hideLoader();
          console.log("Initialized");
        } else {
          this.isInitialized = false;
          if (result.status) {
            throw new Error(result.status);
          }
          console.log("Not Initialized");
        }
      })
      .catch((err) => {
        console.log(err);
        this.isInitialized = false;
        console.log("Not Initialized");
      });
  };
}

const paxSDK = new PaxSDK();
